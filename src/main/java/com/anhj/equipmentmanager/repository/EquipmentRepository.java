package com.anhj.equipmentmanager.repository;

import com.anhj.equipmentmanager.entity.Equipment;
import com.anhj.equipmentmanager.enums.EquipmentType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    List<Equipment> findAllByEquipmentTypeOrderByIdDesc(EquipmentType equipmentType);
}
