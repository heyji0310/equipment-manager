package com.anhj.equipmentmanager.repository;

import com.anhj.equipmentmanager.entity.UsageDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface UsageDetailRepository extends JpaRepository<UsageDetail, Long> {
    List<UsageDetail> findAllByDateEquipmentUsageGreaterThanEqualAndDateEquipmentUsageLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
