package com.anhj.equipmentmanager.repository;

import com.anhj.equipmentmanager.entity.Employee;
import com.anhj.equipmentmanager.enums.DepartmentName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    List<Employee> findAllByDepartmentNameOrderByIdDesc(DepartmentName departmentName);
}
