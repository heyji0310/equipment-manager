package com.anhj.equipmentmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EquipmentType {
    APPARATUS("장비")
    , MATERIALS("자재")
    , PARTS("부품")
    , ETC("기타")
    ;

    private final String equipmentTypeName;
}
