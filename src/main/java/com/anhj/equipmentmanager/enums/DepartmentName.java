package com.anhj.equipmentmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DepartmentName {
    R_D_TEAM("개발팀")
    , MANAGEMENT_SUPPORT_TEAM("경영지원팀")
    , MARKETING_TEAM("마케팅팀")
    , PRODUCTION_TEAM("생산팀")
    , HUMAN_RESOURCES_TEAM("인사관리팀")
    ;

    private final String departName;
}
