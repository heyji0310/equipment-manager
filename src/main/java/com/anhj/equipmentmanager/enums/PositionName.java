package com.anhj.equipmentmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PositionName {
    STAFF ("사원")
    , ASSISTANT_MANAGER("대리")
    , SUPERVISOR("과장")
    , CHIEF_MANAGER("차장")
    , DEPARTMENT_HEAD("부장")
    , RESEARCH_ENGINEER("연구원")
    , ASSOCIATE_ENGINEER("전임연구원")
    , SENIOR_RESEARCH_ENGINEER("책임연구원")
    , PRINCIPAL_RESEARCH_ENGINEER("수석연구원")
    ;

    private final String positName;
}
