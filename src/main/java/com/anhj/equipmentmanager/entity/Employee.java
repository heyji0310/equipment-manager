package com.anhj.equipmentmanager.entity;

import com.anhj.equipmentmanager.enums.DepartmentName;
import com.anhj.equipmentmanager.enums.PositionName;
import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import com.anhj.equipmentmanager.model.EmployeeJoinCompanyRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer employeeNum;

    @Column(nullable = false, length = 20)
    private String employeeName;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private DepartmentName departmentName;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private PositionName positionName;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false, length = 15)
    private String employeePhone;

    @Column(nullable = false, length = 50)
    private String employeeAddress;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false)
    private LocalDate dateJoinCompany;

    private LocalDate dateResignation;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putResignation() {
        this.isEnable = false;
        this.dateResignation = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());
    }

    private Employee(EmployeeBuilder builder) {
        this.employeeNum = builder.employeeNum;
        this.employeeName = builder.employeeName;
        this.departmentName = builder.departmentName;
        this.positionName = builder.positionName;
        this.birthday = builder.birthday;
        this.employeePhone = builder.employeePhone;
        this.employeeAddress = builder.employeeAddress;
        this.isEnable = builder.isEnable;
        this.dateJoinCompany = builder.dateJoinCompany;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class EmployeeBuilder implements CommonModelBuilder<Employee> {
        private final Integer employeeNum;
        private final String employeeName;
        private final DepartmentName departmentName;
        private final PositionName positionName;
        private final LocalDate birthday;
        private final String employeePhone;
        private final String employeeAddress;
        private final Boolean isEnable;
        private final LocalDate dateJoinCompany;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public EmployeeBuilder(EmployeeJoinCompanyRequest joinCompanyRequest) {
            this.employeeNum = joinCompanyRequest.getEmployeeNum();
            this.employeeName = joinCompanyRequest.getEmployeeName();
            this.departmentName = joinCompanyRequest.getDepartmentName();
            this.positionName = joinCompanyRequest.getPositionName();
            this.birthday = joinCompanyRequest.getBirthday();
            this.employeePhone = joinCompanyRequest.getEmployeePhone();
            this.employeeAddress = joinCompanyRequest.getEmployeeAddress();
            this.isEnable = true;
            this.dateJoinCompany = joinCompanyRequest.getDateJoinCompany();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
        @Override
        public Employee build() {
            return new Employee(this);
        }
    }
}
