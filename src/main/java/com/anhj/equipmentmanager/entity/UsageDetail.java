package com.anhj.equipmentmanager.entity;

import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId", nullable = false)
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipmentId", nullable = false)
    private Equipment equipment;

    @Column(nullable = false)
    private LocalDateTime dateEquipmentUsage;

    private UsageDetail(UsageDetailBuilder builder){
        this.employee = builder.employee;
        this.equipment = builder.equipment;
        this.dateEquipmentUsage = builder.dateEquipmentUsage;
    }

    public static class UsageDetailBuilder implements CommonModelBuilder<UsageDetail> {
        private final Employee employee;
        private final Equipment equipment;
        private final LocalDateTime dateEquipmentUsage;

        public UsageDetailBuilder(Employee employee, Equipment equipment, LocalDateTime dateEquipmentUsage) {
            this.employee = employee;
            this.equipment = equipment;
            this.dateEquipmentUsage = dateEquipmentUsage;
        }

        @Override
        public UsageDetail build() {
            return new UsageDetail(this);
        }
    }
}
