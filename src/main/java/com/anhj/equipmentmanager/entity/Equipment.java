package com.anhj.equipmentmanager.entity;

import com.anhj.equipmentmanager.enums.EquipmentType;
import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import com.anhj.equipmentmanager.model.EquipmentRequest;
import com.anhj.equipmentmanager.model.EquipmentStorageLocationUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private EquipmentType equipmentType;

    @Column(nullable = false, length = 30)
    private String equipmentName;

    @Column(nullable = false, length = 30)
    private String tradingCompany;

    @Column(nullable = false)
    private Double equipmentPrice;

    @Column(nullable = false, length = 20)
    private String storageLocation;

    @Column(nullable = false)
    private Boolean isAvailable;

    @Column(nullable = false)
    private LocalDate dateBuy;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putAvailable() {
        this.isAvailable = false;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putUpdateStorageLocation(EquipmentStorageLocationUpdateRequest updateRequest) {
        this.storageLocation = updateRequest.getStorageLocation();
        this.dateUpdate = LocalDateTime.now();
    }

    private Equipment(EquipmentBuilder builder) {
        this.equipmentType = builder.equipmentType;
        this.equipmentName = builder.equipmentName;
        this.tradingCompany = builder.tradingCompany;
        this.equipmentPrice = builder.equipmentPrice;
        this.storageLocation = builder.storageLocation;
        this.isAvailable = builder.isAvailable;
        this.dateBuy = builder.dateBuy;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class EquipmentBuilder implements CommonModelBuilder<Equipment> {
        private final EquipmentType equipmentType;
        private final String equipmentName;
        private final String tradingCompany;
        private final Double equipmentPrice;
        private final String storageLocation;
        private final Boolean isAvailable;
        private final LocalDate dateBuy;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public EquipmentBuilder(EquipmentRequest request) {
            this.equipmentType = request.getEquipmentType();
            this.equipmentName = request.getEquipmentName();
            this.tradingCompany = request.getTradingCompany();
            this.equipmentPrice = request.getEquipmentPrice();
            this.storageLocation = request.getStorageLocation();
            this.isAvailable = true;
            this.dateBuy = request.getDateBuy();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }
        @Override
        public Equipment build() {
            return new Equipment(this);
        }
    }
}
