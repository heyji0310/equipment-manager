package com.anhj.equipmentmanager.controller;

import com.anhj.equipmentmanager.enums.DepartmentName;
import com.anhj.equipmentmanager.model.*;
import com.anhj.equipmentmanager.service.EmployeeService;
import com.anhj.equipmentmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "직원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @ApiOperation(value = "직원 정보 등록")
    @PostMapping("/new")
    public CommonResult setEmployee(@RequestBody @Valid EmployeeJoinCompanyRequest joinCompanyRequest) {
        employeeService.setEmployee(joinCompanyRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 정보 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "직원 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<EmployeeDetail> getEmployee(@PathVariable long id) {
        return ResponseService.getSingleResult(employeeService.getEmployee(id));
    }

    @ApiOperation(value = "부서별 직원 정보 조회")
    @GetMapping("/search")
    public ListResult<EmployeeItem> getEmployees(@RequestParam(value = "departmentName", required = false)DepartmentName departmentName){
        if (departmentName == null) {
            return ResponseService.getListResult(employeeService.getEmployees(), true);
        } else {
            return ResponseService.getListResult(employeeService.getEmployees(departmentName), true);
        }
    }

    @ApiOperation(value = "직원 정보 삭제")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "직원 시퀀스", required = true)
    )
    @DeleteMapping("/{id}")
    public CommonResult delEmployee(@PathVariable long id) {
        employeeService.putResignation(id);
        return ResponseService.getSuccessResult();
    }
}
