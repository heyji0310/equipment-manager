package com.anhj.equipmentmanager.controller;

import com.anhj.equipmentmanager.enums.EquipmentType;
import com.anhj.equipmentmanager.model.*;
import com.anhj.equipmentmanager.service.EquipmentService;
import com.anhj.equipmentmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기자재 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/equipment")
public class EquipmentController {
    private final EquipmentService equipmentService;

    @ApiOperation(value = "기자재 정보 등록")
    @PostMapping("/new")
    public CommonResult setEquipment(@RequestBody @Valid EquipmentRequest request) {
        equipmentService.setEquipment(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기자재 정보 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "기자재 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<EquipmentDetail> getEquipment(@PathVariable long id) {
        return ResponseService.getSingleResult(equipmentService.getEquipment(id));
    }

    @ApiOperation(value = "기자재 정보 전체 조회")
    @GetMapping("/all")
    public ListResult<EquipmentDetail> getAllEquipments() {
        return ResponseService.getListResult(equipmentService.getAllEquipments(), true);
    }

    @ApiOperation(value = "기자재 종류 별 정보 조회")
    @GetMapping("/search")
    public ListResult<EquipmentItem> getEquipments(@RequestParam(value = "equipmentType", required = false) EquipmentType equipmentType) {
        if (equipmentType == null) {
            return ResponseService.getListResult(equipmentService.getEquipments(), true);
        } else {
            return ResponseService.getListResult(equipmentService.getEquipments(equipmentType), true);
        }
    }

    @ApiOperation(value = "기자재 보관위치 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "기자재 시퀀스", required = true)
    )
    @PutMapping("/{id}")
    public CommonResult putUpdateStorageLocation(@PathVariable long id, @RequestBody @Valid EquipmentStorageLocationUpdateRequest updateRequest) {
        equipmentService.putUpdateStorageLocation(id, updateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기자재 삭제")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "기자재 시퀀스", required = true)
    )
    @DeleteMapping("/{id}")
    public CommonResult delEquipment(@PathVariable long id) {
        equipmentService.putIsAvailable(id);
        return ResponseService.getSuccessResult();
    }
}
