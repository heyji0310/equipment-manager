package com.anhj.equipmentmanager.controller;

import com.anhj.equipmentmanager.entity.Employee;
import com.anhj.equipmentmanager.entity.Equipment;
import com.anhj.equipmentmanager.model.CommonResult;
import com.anhj.equipmentmanager.model.ListResult;
import com.anhj.equipmentmanager.model.UsageDetailItem;
import com.anhj.equipmentmanager.model.UsageDetailRequest;
import com.anhj.equipmentmanager.service.EmployeeService;
import com.anhj.equipmentmanager.service.EquipmentService;
import com.anhj.equipmentmanager.service.ResponseService;
import com.anhj.equipmentmanager.service.UsageDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "기자재 이용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-detail")
public class UsageDetailController {
    private final EmployeeService employeeService;
    private final EquipmentService equipmentService;
    private final UsageDetailService usageDetailService;

    @ApiOperation(value = "기자재 이용내역 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetail(@RequestBody @Valid UsageDetailRequest request) {
        Employee employee = employeeService.getEmployeeData(request.getEmployeeId());
        Equipment equipment = equipmentService.getEquipmentData(request.getEquipmentId());

        usageDetailService.setUsageDetail(employee, equipment, request.getDateEquipmentUsage());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기자재 이용내역 전체 조회")
    @GetMapping("/all")
    public ListResult<UsageDetailItem> getAllUsageDetails() {
        return ResponseService.getListResult(usageDetailService.getAllUsageDetails(), true);
    }

    @ApiOperation(value = "기간 내 이용내역 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dateStart", value = "시작일", required = true),
            @ApiImplicitParam(name = "dateEnd", value = "종료일", required = true)
    })
    @GetMapping("/search")
    public ListResult<UsageDetailItem> getUsageDetails(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(name = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
            ) {
        return ResponseService.getListResult(usageDetailService.getUsageDetails(dateStart, dateEnd), true);
    }

}
