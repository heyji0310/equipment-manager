package com.anhj.equipmentmanager.model;

import com.anhj.equipmentmanager.entity.Employee;
import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EmployeeItem {
    @ApiModelProperty(notes = "직원 시퀀스")
    private Long employeeId;

    @ApiModelProperty(notes = "직원번호")
    private Integer employeeNum;

    @ApiModelProperty(notes = "[부서] 직원이름 + 직급")
    private String employeeDepartmentPositionFullName;

    @ApiModelProperty(notes = "직원 연락처")
    private String employeePhone;

    @ApiModelProperty(notes = "직원 재직여부")
    private String isEnable;

    private EmployeeItem(EmployeeItemBuilder builder) {
        this.employeeId = builder.employeeId;
        this.employeeNum = builder.employeeNum;
        this.employeeDepartmentPositionFullName = builder.employeeDepartmentPositionFullName;
        this.employeePhone = builder.employeePhone;
        this.isEnable = builder.isEnable;
    }

    public static class EmployeeItemBuilder implements CommonModelBuilder<EmployeeItem> {
        private final Long employeeId;
        private final Integer employeeNum;
        private final String employeeDepartmentPositionFullName;
        private final String employeePhone;
        private final String isEnable;

        public EmployeeItemBuilder(Employee employee) {
            this.employeeId = employee.getId();
            this.employeeNum = employee.getEmployeeNum();
            this.employeeDepartmentPositionFullName = "[" + employee.getDepartmentName() + "] " + employee.getEmployeeName() + " " + employee.getPositionName();
            this.employeePhone = employee.getEmployeePhone();
            this.isEnable = employee.getIsEnable() ? "재직중" : "퇴사";
        }
        @Override
        public EmployeeItem build() {
            return new EmployeeItem(this);
        }
    }
}
