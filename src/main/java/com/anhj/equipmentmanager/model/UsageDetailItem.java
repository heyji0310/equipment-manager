package com.anhj.equipmentmanager.model;

import com.anhj.equipmentmanager.entity.UsageDetail;
import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailItem {

    @ApiModelProperty(notes = "기자재 사용내역 시퀀스")
    private Long usageDetailId;
    @ApiModelProperty(notes = "기자재 사용일자")
    private LocalDateTime dateEquipmentUsage;

    @ApiModelProperty(notes = "직원 시퀀스")
    private Long employeeId;
    @ApiModelProperty(notes = "직원 정보([부서] 이름 + 직급)")
    private String employeeFullName;

    @ApiModelProperty(notes = "직원 재직 여부")
    private String employeeIsEnable;

    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long equipmentId;
    @ApiModelProperty(notes = "기자재 정보([기자재타입] + 기자재 이름)")
    private String equipmentFullInfo;

    @ApiModelProperty(notes = "기자재 사용가능 여부")
    private String equipmentIsAvailable;

    private UsageDetailItem(UsageDetailItemBuilder builder) {
        this.usageDetailId = builder.usageDetailId;
        this.dateEquipmentUsage = builder.dateEquipmentUsage;
        this.employeeId = builder.employeeId;
        this.employeeFullName = builder.employeeFullName;
        this.employeeIsEnable = builder.employeeIsEnable;
        this.equipmentId = builder.equipmentId;
        this.equipmentFullInfo = builder.equipmentFullInfo;
        this.equipmentIsAvailable = builder.equipmentIsAvailable;
    }

    public static class UsageDetailItemBuilder implements CommonModelBuilder<UsageDetailItem> {
        private final Long usageDetailId;
        private final LocalDateTime dateEquipmentUsage;
        private final Long employeeId;
        private final String employeeFullName;
        private final String employeeIsEnable;
        private final Long equipmentId;
        private final String equipmentFullInfo;
        private final String equipmentIsAvailable;

        public UsageDetailItemBuilder(UsageDetail usageDetail) {
            this.usageDetailId = usageDetail.getId();
            this.dateEquipmentUsage = usageDetail.getDateEquipmentUsage();
            this.employeeId = usageDetail.getEmployee().getId();
            this.employeeFullName = "[" + usageDetail.getEmployee().getDepartmentName().getDepartName() + "] " + usageDetail.getEmployee().getEmployeeName() + " " + usageDetail.getEmployee().getPositionName().getPositName();
            this.employeeIsEnable = usageDetail.getEmployee().getIsEnable() ? "재직중" : "퇴사";
            this.equipmentId = usageDetail.getEquipment().getId();
            this.equipmentFullInfo = "[" + usageDetail.getEquipment().getEquipmentType().getEquipmentTypeName() + "] " + usageDetail.getEquipment().getEquipmentName();
            this.equipmentIsAvailable = usageDetail.getEquipment().getIsAvailable() ? "사용 가능" : "사용 불가능";
        }

        @Override
        public UsageDetailItem build() {
            return new UsageDetailItem(this);
        }
    }
}
