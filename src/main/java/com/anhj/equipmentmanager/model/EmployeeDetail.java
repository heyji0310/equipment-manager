package com.anhj.equipmentmanager.model;

import com.anhj.equipmentmanager.entity.Employee;
import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EmployeeDetail {
    private Long id;
    private Integer employeeNum;
    private String employeeName;
    private String departmentName;
    private String positionName;
    private LocalDate birthday;
    private String employeePhone;
    private String employeeAddress;
    private String isEnable;
    private LocalDate dateJoinCompany;

    private EmployeeDetail(EmployeeDetailBuilder builder) {
        this.id = builder.id;
        this.employeeNum = builder.employeeNum;
        this.employeeName = builder.employeeName;
        this.departmentName = builder.departmentName;
        this.positionName = builder.positionName;
        this.birthday = builder.birthday;
        this.employeePhone = builder.employeePhone;
        this.employeeAddress = builder.employeeAddress;
        this.isEnable = builder.isEnable;
        this.dateJoinCompany = builder.dateJoinCompany;
    }

    public static class EmployeeDetailBuilder implements CommonModelBuilder<EmployeeDetail> {
        private final Long id;
        private final Integer employeeNum;
        private final String employeeName;
        private final String departmentName;
        private final String positionName;
        private final LocalDate birthday;
        private final String employeePhone;
        private final String employeeAddress;
        private final String isEnable;
        private final LocalDate dateJoinCompany;

        public EmployeeDetailBuilder(Employee employee) {
            this.id = employee.getId();
            this.employeeNum = employee.getEmployeeNum();
            this.employeeName = employee.getEmployeeName();
            this.departmentName = employee.getDepartmentName().getDepartName();
            this.positionName = employee.getPositionName().getPositName();
            this.birthday = employee.getBirthday();
            this.employeePhone = employee.getEmployeePhone();
            this.employeeAddress = employee.getEmployeeAddress();
            this.isEnable = employee.getIsEnable() ? "재직중" : "퇴사";
            this.dateJoinCompany = employee.getDateJoinCompany();
        }

        @Override
        public EmployeeDetail build() {
            return new EmployeeDetail(this);
        }
    }
}
