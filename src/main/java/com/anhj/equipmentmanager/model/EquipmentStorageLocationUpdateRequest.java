package com.anhj.equipmentmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EquipmentStorageLocationUpdateRequest {
    @ApiModelProperty(notes = "보관 위치")
    private String storageLocation;

}
