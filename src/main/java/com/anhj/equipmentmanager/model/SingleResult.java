package com.anhj.equipmentmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult { //  레포지터리 T, ID 참조 T = 타입의 약자 / <>제네릭
    private T data; // SingleResult<T>의 이름 입력
}