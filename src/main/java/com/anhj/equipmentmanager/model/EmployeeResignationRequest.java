package com.anhj.equipmentmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class EmployeeResignationRequest {
    @ApiModelProperty(notes = "퇴사일자")
    @NotNull
    private LocalDate dateResignation;
}
