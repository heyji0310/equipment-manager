package com.anhj.equipmentmanager.model;

import com.anhj.equipmentmanager.entity.Equipment;
import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentItem {
    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "기자재 풀네임([기자재타입] + 기자재 이름)")
    private String equipmentFullName;

    @ApiModelProperty(notes = "보관위치")
    private String storageLocation;

    @ApiModelProperty(notes = "사용 가능 여부")
    private String isAvailable;

    @ApiModelProperty(notes = "구매일자")
    private LocalDate dateBuy;

    private EquipmentItem(EquipmentItemBuilder builder) {
        this.id = builder.id;
        this.equipmentFullName = builder.equipmentFullName;
        this.storageLocation = builder.storageLocation;
        this.isAvailable = builder.isAvailable;
        this.dateBuy = builder.dateBuy;
    }

    public static class EquipmentItemBuilder implements CommonModelBuilder<EquipmentItem> {
        private final Long id;
        private final String equipmentFullName;
        private final String storageLocation;
        private final String isAvailable;
        private final LocalDate dateBuy;

        public EquipmentItemBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.equipmentFullName = "[" + equipment.getEquipmentType().getEquipmentTypeName() + "] " + equipment.getEquipmentName();
            this.storageLocation = equipment.getStorageLocation();
            this.isAvailable = equipment.getIsAvailable() ? "사용 가능" : "사용 불가능";
            this.dateBuy = equipment.getDateBuy();
        }
        @Override
        public EquipmentItem build() {
            return new EquipmentItem(this);
        }
    }
}
