package com.anhj.equipmentmanager.model;

import com.anhj.equipmentmanager.enums.DepartmentName;
import com.anhj.equipmentmanager.enums.PositionName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class EmployeeJoinCompanyRequest {
    @ApiModelProperty(notes = "사원번호")
    @NotNull
    private Integer employeeNum;

    @ApiModelProperty(notes = "사원이름")
    @NotNull
    @Length(min = 2, max = 20)
    private String employeeName;

    @ApiModelProperty(notes = "부서명")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DepartmentName departmentName;

    @ApiModelProperty(notes = "직급")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PositionName positionName;

    @ApiModelProperty(notes = "직원 생년월일")
    @NotNull
    private LocalDate birthday;

    @ApiModelProperty(notes = "직원 연락처")
    @NotNull
    @Length(min = 10, max = 15)
    private String employeePhone;

    @ApiModelProperty(notes = "직원 주소")
    @NotNull
    @Length(max = 50)
    private String employeeAddress;

    @ApiModelProperty(notes = "입사일자")
    @NotNull
    private LocalDate dateJoinCompany;
}
