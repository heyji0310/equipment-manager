package com.anhj.equipmentmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class UsageDetailRequest {
    @ApiModelProperty(notes = "직원 id", required = true)
    @NotNull
    private Long employeeId;

    @ApiModelProperty(notes = "기자재 id", required = true)
    @NotNull
    private Long equipmentId;

    @ApiModelProperty(notes = "이용시간", required = true)
    @NotNull
    private LocalDateTime dateEquipmentUsage;
}
