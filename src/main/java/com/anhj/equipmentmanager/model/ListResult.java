package com.anhj.equipmentmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult { // repository와 비슷
    private List<T> list; // ex) 콜센터: 동일하게 대응하기 위해 규칙을 만드는 것 / T : 정해지지 않은 무언가

    private Long totalItemCount; // 아이템의 총 갯수

    private Integer totalPage; // 총 페이지 수

    private Integer currentPage; // 현재 페이지
}