package com.anhj.equipmentmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CommonResult { // enums에 있는 것들을 가지고 옴
    private Boolean isSuccess;
    private Integer code;
    private String msg;
}