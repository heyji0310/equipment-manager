package com.anhj.equipmentmanager.model;

import com.anhj.equipmentmanager.enums.EquipmentType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class EquipmentRequest {
    @ApiModelProperty(notes = "기자재 타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private EquipmentType equipmentType;

    @ApiModelProperty(notes = "기자재 이름", required = true)
    @NotNull
    @Length(max = 30)
    private String equipmentName;

    @ApiModelProperty(notes = "구매처", required = true)
    @NotNull
    @Length(max = 30)
    private String tradingCompany;

    @ApiModelProperty(notes = "기자재 가격", required = true)
    @NotNull
    private Double equipmentPrice;

    @ApiModelProperty(notes = "보관 위치", required = true)
    @NotNull
    @Length(max = 20)
    private String storageLocation;

    @ApiModelProperty(notes = "구매일자", required = true)
    @NotNull
    private LocalDate dateBuy;
}
