package com.anhj.equipmentmanager.model;

import com.anhj.equipmentmanager.entity.Equipment;
import com.anhj.equipmentmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentDetail {
    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "기자재 종류")
    private String equipmentType;

    @ApiModelProperty(notes = "기자재 이름")
    private String equipmentName;

    @ApiModelProperty(notes = "기자재 구매처")
    private String tradingCompany;

    @ApiModelProperty(notes = "기자재 가격")
    private Double equipmentPrice;

    @ApiModelProperty(notes = "보관위치")
    private String storageLocation;

    @ApiModelProperty(notes = "사용 가능 여부")
    private String isAvailable;

    @ApiModelProperty(notes = "구매일자")
    private LocalDate dateBuy;

    private EquipmentDetail(EquipmentDetailBuilder builder) {
        this.id = builder.id;
        this.equipmentType = builder.equipmentType;
        this.equipmentName = builder.equipmentName;
        this.tradingCompany = builder.tradingCompany;
        this.equipmentPrice = builder.equipmentPrice;
        this.storageLocation = builder.storageLocation;
        this.isAvailable = builder.isAvailable;
        this.dateBuy = builder.dateBuy;
    }

    public static class EquipmentDetailBuilder implements CommonModelBuilder<EquipmentDetail> {
        private final Long id;
        private final String  equipmentType;
        private final String equipmentName;

        private final String tradingCompany;
        private final Double equipmentPrice;
        private final String storageLocation;
        private final String isAvailable;
        private final LocalDate dateBuy;

        public EquipmentDetailBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.equipmentType = equipment.getEquipmentType().getEquipmentTypeName();
            this.equipmentName = equipment.getEquipmentName();
            this.tradingCompany = equipment.getTradingCompany();
            this.equipmentPrice = equipment.getEquipmentPrice();
            this.storageLocation = equipment.getStorageLocation();
            this.isAvailable = equipment.getIsAvailable() ? "사용 가능" : "사용 불가능";
            this.dateBuy = equipment.getDateBuy();
        }
        @Override
        public EquipmentDetail build() {
            return new EquipmentDetail(this);
        }
    }
}
