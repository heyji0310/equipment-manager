package com.anhj.equipmentmanager.service;

import com.anhj.equipmentmanager.entity.Employee;
import com.anhj.equipmentmanager.enums.DepartmentName;
import com.anhj.equipmentmanager.exception.CMissingDataException;
import com.anhj.equipmentmanager.exception.CNoMemberDataException;
import com.anhj.equipmentmanager.model.*;
import com.anhj.equipmentmanager.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public Employee getEmployeeData(long id) {
        return employeeRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setEmployee(EmployeeJoinCompanyRequest joinCompanyRequest) {
        Employee employee = new Employee.EmployeeBuilder(joinCompanyRequest).build();
        employeeRepository.save(employee);
    }

    public EmployeeDetail getEmployee(long id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new EmployeeDetail.EmployeeDetailBuilder(employee).build();
    }
    public ListResult<EmployeeItem> getEmployees() {
        List<Employee> employees = employeeRepository.findAll();

        List<EmployeeItem> result = new LinkedList<>();

        employees.forEach(employee -> {
            EmployeeItem addItem = new EmployeeItem.EmployeeItemBuilder(employee).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }


    public ListResult<EmployeeItem> getEmployees(DepartmentName departmentName) {
        List<Employee> employees = employeeRepository.findAllByDepartmentNameOrderByIdDesc(departmentName);

        List<EmployeeItem> result = new LinkedList<>();

        employees.forEach(employee -> {
            EmployeeItem addItem = new EmployeeItem.EmployeeItemBuilder(employee).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public void putResignation(long id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!employee.getIsEnable()) throw new CNoMemberDataException();

        employee.putResignation();
        employeeRepository.save(employee);
    }
}
