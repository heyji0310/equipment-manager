package com.anhj.equipmentmanager.service;

import com.anhj.equipmentmanager.entity.Employee;
import com.anhj.equipmentmanager.entity.Equipment;
import com.anhj.equipmentmanager.entity.UsageDetail;
import com.anhj.equipmentmanager.model.ListResult;
import com.anhj.equipmentmanager.model.UsageDetailItem;
import com.anhj.equipmentmanager.repository.UsageDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailService {
    private final UsageDetailRepository usageDetailRepository;

    public void setUsageDetail(Employee employee, Equipment equipment, LocalDateTime dateEquipmentUsage) {
        UsageDetail usageDetail = new UsageDetail.UsageDetailBuilder(employee, equipment, dateEquipmentUsage).build();
        usageDetailRepository.save(usageDetail);
    }

    public ListResult<UsageDetailItem> getAllUsageDetails() {
        List<UsageDetail> usageDetails = usageDetailRepository.findAll();

        List<UsageDetailItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetail -> {
            UsageDetailItem addItem = new UsageDetailItem.UsageDetailItemBuilder(usageDetail).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<UsageDetailItem> getUsageDetails(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );

        List<UsageDetail> usageDetails = usageDetailRepository.findAllByDateEquipmentUsageGreaterThanEqualAndDateEquipmentUsageLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<UsageDetailItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetail -> {
            UsageDetailItem addItem = new UsageDetailItem.UsageDetailItemBuilder(usageDetail).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
