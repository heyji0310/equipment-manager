package com.anhj.equipmentmanager.service;

import com.anhj.equipmentmanager.enums.ResultCode;
import com.anhj.equipmentmanager.model.CommonResult;
import com.anhj.equipmentmanager.model.ListResult;
import com.anhj.equipmentmanager.model.SingleResult;
import org.springframework.stereotype.Service;

@Service  // 협업 하지 않기 때문에 @RequiredArgsConstructor 를 사용하지 않음
          // ResponseService : 공통 응답
          // setSuccessResult : 성공 응답
public class ResponseService {
    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {  // ()안의 ListResult<T> 에서 CommonResult 를 가져온다
        if (isSuccess) setSuccessResult(result); // CommonResult 상속했기 때문에 result 가능
        else setFailResult(result);

        return result;
    }

    // T<제네릭>: 무언가 하나를 받을거야 / model SingleResult<T> 받아옴 : 무언가 받아서 처리할게
    // static <T> 를 받는데 그건 data 이다 (T data) / T(타입), data(이름)
    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setData(data);  // (T data) 에서 data 를 받아옴
        setSuccessResult(result);  // 아래에서 steSuccessResult 를 불러온다

        return result;

    }

    public static CommonResult getSuccessResult() {  // 성공은 한가지 유형 이므로 () 에 내용이 없다
        CommonResult result = new CommonResult();
        setSuccessResult(result);

        return result;
    }

    public static CommonResult getFailResult(ResultCode resultCode) {  // 실패 유형은 여러가지이므로 ()에 내용이 있다
        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());

        return result;
    }

    private static void setSuccessResult(CommonResult result) {  // SuccessResult 를 자주 사용하므로 static 으로 RAM 에 고정 / 성공에 대한 결과값
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }

    private static void setFailResult(CommonResult result) {  //  실패에 대한 결과값
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }

}