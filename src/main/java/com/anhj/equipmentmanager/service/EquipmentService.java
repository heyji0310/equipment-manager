package com.anhj.equipmentmanager.service;

import com.anhj.equipmentmanager.entity.Equipment;
import com.anhj.equipmentmanager.enums.EquipmentType;
import com.anhj.equipmentmanager.exception.CMissingDataException;
import com.anhj.equipmentmanager.exception.CNoMemberDataException;
import com.anhj.equipmentmanager.model.*;
import com.anhj.equipmentmanager.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    public Equipment getEquipmentData(long id) {
        return equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
    public void setEquipment(EquipmentRequest request) {
        Equipment equipment = new Equipment.EquipmentBuilder(request).build();
        equipmentRepository.save(equipment);
    }

    public EquipmentDetail getEquipment(long id) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new EquipmentDetail.EquipmentDetailBuilder(equipment).build();
    }

    public ListResult<EquipmentDetail> getAllEquipments() {
        List<Equipment> allEquipments = equipmentRepository.findAll();

        List<EquipmentDetail> result = new LinkedList<>();

        allEquipments.forEach(equipment -> {
            EquipmentDetail addItem = new EquipmentDetail.EquipmentDetailBuilder(equipment).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<EquipmentItem> getEquipments() {
        List<Equipment> equipments = equipmentRepository.findAll();

        List<EquipmentItem> result = new LinkedList<>();

        equipments.forEach(equipment -> {
            EquipmentItem addItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<EquipmentItem> getEquipments(EquipmentType equipmentType) {
        List<Equipment> equipments = equipmentRepository.findAllByEquipmentTypeOrderByIdDesc(equipmentType);

        List<EquipmentItem> result = new LinkedList<>();

        equipments.forEach(equipment -> {
            EquipmentItem addItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putUpdateStorageLocation(long id, EquipmentStorageLocationUpdateRequest updateRequest) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        equipment.putUpdateStorageLocation(updateRequest);
        equipmentRepository.save(equipment);
    }

    public void putIsAvailable(long id) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!equipment.getIsAvailable()) throw new CNoMemberDataException();

        equipment.putAvailable();
        equipmentRepository.save(equipment);
    }
}
