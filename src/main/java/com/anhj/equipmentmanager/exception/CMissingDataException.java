package com.anhj.equipmentmanager.exception;

 // 클래스(CMissingDataException)에서 C : 커스텀의 약자 / 나만의 출구 -> 에러가 났을 때 던진다.
public class CMissingDataException extends RuntimeException {
    public CMissingDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMissingDataException(String msg) {
        super(msg);
    }

    public CMissingDataException() {
        super();
    }
}